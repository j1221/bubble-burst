# Bubble Burst [2011]

Created a Bubbles Game with JavaScript and HTML5.

## Playable Example

[Play Bubble Burst](https://majorvitec.itch.io/bubble-burst)

## Game Overview

### Playing Screen

![Playing Screen](/images/readme/play_screen.png "Playing Screen")

