﻿window.onload = function ()
{
	alert("Bubbles");
	var canvas = document.getElementById("bubbles-screen");
	var width = canvas.width;
	var	height = canvas.height;
	var	ctx = canvas.getContext('2d');
	var FPS = 20;
	var ADD = 0.5;
	
	function randHex ()
	{
		return "0123456789abcdef"[Math.floor(Math.random()*16)];
	};
	
	function drawCircle (x, y, r, c)
	{
		ctx.fillStyle = c;  //hallo 
		ctx.beginPath();
		ctx.arc(x, y, r, 0, Math.PI*2, true);
		ctx.fill(); //fuehlen des Kreises
		
		ctx.beginPath(); //neuer Startpunkt
		ctx.arc(x, y, r, 0, Math.PI*2, true);
		ctx.stroke(); //Rahmen um den Kreis
		
		//shine
		ctx.fillStyle = "rgba(255,255,255,0.4);";
		ctx.beginPath();
		ctx.arc(x-(r*0.5), y-(r*0.5), r*0.2, 0, Math.PI*2, true);
		ctx.fill();
	};
	
	function dist (a,b)
	{
		 return Math.sqrt((a.x-b.x) * (a.x-b.x) + (a.y-b.y)*(a.y-b.y)); 
	};
	
	var circles = [];
	
	function drawBubbles ()
	{
		ctx.clearRect(0,0,width,height);
		for (var i = 0, l = circles.length; i < l; i++)
		{
			drawCircle(circles[i].x, circles[i].y, circles[i].r, circles[i].c);
		}
	};
	
	canvas.onclick = function (ev)
	{
		var x = ev.clientX;
		var y = ev.clientY;
		
		
		
		for(var i = 0, l = circles.length; i < l; i++)
		{
			if(dist({x:x, y:y}, circles[i]) < circles[i].r)
			{
				circles.splice(i,1);
				//warning: this destructively modiifies the list we're iterating over
				break;
			}
		}
	};
	
	var addTimer = setInterval
	(	function addCircles ()
		{
			//Add the circle if we're outside the range of all the others
			var newCircle = { 
								x: Math.random()*width,
								y: Math.random()*height,
								r: 0,
								c: "#" + randHex() + randHex() + randHex()
							}; 
			for (var i = 0, l = circles.length; i<l; i++)
			{
				if(dist(circles[i], newCircle) < 150 + circles[i].r)
				{
					//vermeide
					return addCircles();
				}
			}
			circles.push(newCircle);
		}, 1000*ADD	
	);
	
	var gameTimer =  setInterval
	(	function ()
		{ 
			//lass Bubbles wachsen
			circles = circles.map(function(circle){ 
				circle.r += 1;
				return circle;
			});
			//suche Kollision zwischen Kreisen 
			for(var a = 0, la=circles.length; a<la; a++)
			{
				for(var b = 0, lb=circles.length; b<lb; b++)
				{
					var ca = circles[a], cb = circles[b];
					if(a!=b && dist(ca,cb) <= ca.r + cb.r )
					{
						alert("GAME OVER! Too slow.");
						clearInterval(gameTimer);
						clearInterval(addTimer);
						return;
					}
				}
			}			
			//zeichne Bubbles
			drawBubbles(circles);					   
			}, 1000/FPS	
	);	
};